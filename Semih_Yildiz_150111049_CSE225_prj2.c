#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX 500
#define SUCCESS 0
#define ERROR 1

typedef struct album {
    char albumName[MAX];
    char singerName[MAX];
    int year;
    struct album* LEFT;
    struct album* RIGHT;
    struct song* HEADSONG;
}album;

typedef struct song {
    char songName[MAX];
    int length;
    struct song *left;
    struct song *right;
}song;

album *rut=NULL;
album *new(){
        return (album *)malloc(sizeof(album));
}
song *newSong(){
        return (song * )malloc(sizeof(song));
}
album *root(char albumTitle[MAX],char singerName[MAX],int releaseYear)
{
  album *ptr = new();
        strcpy(ptr->albumName,albumTitle);
        strcpy(ptr->singerName,singerName);
        ptr-> year = releaseYear;
        ptr-> LEFT = NULL;
        ptr-> RIGHT = NULL;
        ptr-> HEADSONG=NULL;
        return ptr;
}
song *nodeSong(char songName[],int length){
  song *ptr = newSong();
        strcpy(ptr->songName,songName);
        ptr->length=length;
        ptr->left=NULL;
        ptr->right=NULL;
        return ptr;
}
album *nodeAlbum(char albumTitle[],char singerName[],int releaseYear){
  album *temp=new();
        strcpy(temp->albumName,albumTitle);
        strcpy(temp->singerName,singerName);
        temp-> year = releaseYear;
        temp-> LEFT = NULL;
        temp-> RIGHT = NULL;
        temp-> HEADSONG=NULL;
        return temp;
} 
void addAlbum(album* ptr,char albumTitle[MAX],char singerName[MAX],int releaseYear)
{
    if(ptr==NULL){
          rut=root(albumTitle,singerName,releaseYear);
        
    }
    else  if(releaseYear >= ptr->year){
          if(ptr->RIGHT){
                  addAlbum(ptr->RIGHT,albumTitle,singerName,releaseYear);
          }
          else{
                 ptr->RIGHT=nodeAlbum(albumTitle,singerName,releaseYear);
          }
      }
    else{
          if(ptr->LEFT){
                  addAlbum(ptr->LEFT,albumTitle,singerName,releaseYear);
          }
          else{ 
                  ptr->LEFT=nodeAlbum(albumTitle,singerName,releaseYear);
          }
      }
}
album *search(album *ptr,char albumTitle[MAX],int releaseYear){
        if(ptr){
                if(ptr->year == releaseYear && !strcmp(ptr->albumName,albumTitle)) return ptr;
                else if( ptr->year <= releaseYear ) return search(ptr->RIGHT,albumTitle,releaseYear);
                else return search(ptr->LEFT,albumTitle,releaseYear);
        }else return 0;
}


int albumNTY(album *ptr,char albumTitle[MAX])
{
  if(ptr)
  {
     albumNTY(ptr->LEFT,albumTitle);
      if(!strcmp( ptr->albumName, albumTitle))
      {
        return ptr->year;
      }
      albumNTY(ptr->RIGHT,albumTitle);
  }
}
void addSong(song *ptr,char albumTitle[MAX],char songName[MAX],int songLength){
  
  if(ptr==NULL){
    search(rut,albumTitle,albumNTY(rut,albumTitle))->HEADSONG=nodeSong(songName,songLength);
  }
  else if(strcmp(songName, ptr->songName) >= 0){
    if(ptr->right){
      addSong(ptr->right,albumTitle,songName,songLength);
    }
    else{
      ptr->right=nodeSong(songName,songLength);
    }
  }
  else{
    if(ptr->left){
      addSong(ptr->left,albumTitle,songName,songLength);
    }
    else{
      ptr->left=nodeSong(songName,songLength);
    }
  }
  
}
album* FindMin(album *node)
{
        if(node){
        if(node->LEFT) /* Go to the left sub tree to find the min element */
                return FindMin(node->LEFT);
        else 
                return node;
        }else return 0;
}
song* FindMinSong(song *node)
{
        if(node){
        if(node->left) /* Go to the left sub tree to find the min element */
                return FindMinSong(node->left);
        else 
                return node;
        }else return 0;
}

album  *removeAlbum(char albumTitle[MAX], album *root){
   album *holder;
        
        if(!root) {
          printf("Element not found\n");
        }
        else if(albumNTY(root,albumTitle) < root->year){
          root->LEFT = removeAlbum(albumTitle,root->LEFT);
        }
        else if(albumNTY(root,albumTitle) > root->year ){
          root->RIGHT = removeAlbum(albumTitle,root->RIGHT);
        }
        else if(albumNTY(root,albumTitle) == root->year && strcmp(root->albumName,albumTitle)){
           root->RIGHT = removeAlbum(albumTitle,root->RIGHT);
        }
        else if(albumNTY(root,albumTitle) == root->year) {
          if(root->RIGHT && root->LEFT){
            holder=FindMin(root->RIGHT);
            root->year=holder->year;
            root->RIGHT=removeAlbum(albumTitle,root->RIGHT);
            
          }
          else if(root->RIGHT==NULL && root->LEFT==NULL){
            root=NULL;
          }
          else{
            holder=root;
             if(root->LEFT==NULL)
              root=root->RIGHT;
            else if(root->RIGHT==NULL)
              root=root->LEFT;
            free(holder);
          }
        }
        return root;
}
song *removeSong(char albumTitle[MAX],char songName[],song *root){
    song *holder;
        
        if(!root) {
          printf("Element not found\n");
        }

        if(strcmp(songName, root->songName) < 0){
          root->left = removeSong(albumTitle,songName,root->left);
        }
        else if(strcmp(songName, root->songName) > 0){
          root->right = removeSong(albumTitle,songName,root->right);
        }
        else{
          if(root->right && root->left){
            holder=FindMinSong(root->right);
            strcpy(root->songName,holder->songName);
            root->right=removeSong(albumTitle,songName,root->right);
          }
          else if(root->right==NULL && root->left==NULL){
            root=NULL;
          }
          else{
            holder=root;
            if(root->left==NULL)
              root=root->right;
            else if(root->right==NULL)
              root=root->left;
            free(holder);
          }
        }
        return root;
}
int CONTROL(album *ptr,char albumTitle[])
{
    
   if (ptr)
    {
    CONTROL(ptr->LEFT,albumTitle);
    if(!strcmp(ptr->albumName, albumTitle))
    {
        return 1;
    }
    CONTROL(ptr->RIGHT, albumTitle);
    }
}
void showAllAlbums(album *ptr){
        if(ptr){
                showAllAlbums( ptr-> LEFT);
                printf("Album Name = %s , Album Release Year %d , Singer Name = %s\n",ptr-> albumName,ptr->year,ptr->singerName);
                showAllAlbums( ptr-> RIGHT);
        }
}
void showAlbumDetails(song *songptr){
  if(songptr){
    showAlbumDetails(songptr->left);
    printf("Song Name = %s, Song Length = %d\n",songptr->songName,songptr->length);
    showAlbumDetails(songptr->right);
  }
}
void showAllSongs(song *ptr){
  if(ptr){
    showAllSongs(ptr->left);
    printf("Song name = %s , songLength = %d \n", ptr->songName,ptr->length);
    showAllSongs(ptr->right);
  }
}
void showListofAllSongs(album *node)
{
     if(node){
      showListofAllSongs(node->LEFT);
      showAllSongs(node->HEADSONG);
      showListofAllSongs(node->RIGHT);
     }
}
showAlbumsInBetweenParticularYears(album *root,int UpperBoundary,int LowerBoundary){
 if(root){
  showAlbumsInBetweenParticularYears(root->LEFT,UpperBoundary,LowerBoundary);
  if(root->year < UpperBoundary && root->year > LowerBoundary){
    printf("%s , %s , %d\n",root->albumName,root->singerName,root->year );
  }
  showAlbumsInBetweenParticularYears(root->RIGHT,UpperBoundary,LowerBoundary);
 }
}
void showSongsWithParticularLengthIN(album *node , song *ptr,int UpperBoundary,int LowerBoundary){
  if(ptr){
    showSongsWithParticularLengthIN(node,ptr->left,UpperBoundary,LowerBoundary);
    if(ptr->length<UpperBoundary && ptr->length>LowerBoundary)
      printf("Song Name = %s , Song Length = %d , Singer Name = %s\n",ptr->songName,ptr->length,node->singerName );
    showSongsWithParticularLengthIN(node,ptr->right,UpperBoundary,LowerBoundary);
  }
}
void showSongsWithParticularLength(album *ptr, int UpperBoundary , int LowerBoundary){
  if(ptr){
  showAlbumsInBetweenParticularYears(ptr->LEFT,UpperBoundary,LowerBoundary);
  showSongsWithParticularLengthIN(ptr,ptr->HEADSONG,UpperBoundary,LowerBoundary);
  showAlbumsInBetweenParticularYears(ptr->RIGHT,UpperBoundary,LowerBoundary);
  }
}
void instructions(  ){
    printf("*************************\n ");
    printf("1:Add an album\n ");
    printf("2:Remove an album\n ");
    printf("3:Show the Album-Binary-Search-Tree\n ");
    printf("4:Show detailed information about a particular album\n ");
    printf("5:Add a song:\n ");
    printf("6:Remove a song from the song list of a album\n " );
    printf("7:Show the List of All Songs\n ");
    printf("8:Query the albums which were released between particular years\n ");
    printf("9: Query the songs whose lengthsare in a particular scope:\n");
    printf("Exit\n");
    printf("*************************\n ");
}
int main(){
    int choice = 1;
    char albumTitle[MAX]="";
    char singerName[MAX]="";
    char songName[MAX]="";
    int songLength;
    int LowerBoundary,UpperBoundary;
    int releaseYear;
    while(1)
    { 
        instructions();
        scanf("%d",&choice);
        if(choice==1)
        {
            printf("Album Title=");
             scanf(" %[^\n]%*c",&albumTitle);
            printf("SingerName=");
             scanf (" %[^\n]%*c",&singerName);
            printf("Release Year=");
             scanf("%d",&releaseYear);

              if(CONTROL(rut,albumTitle)!=1){
              addAlbum(rut,albumTitle,singerName,releaseYear );
              }
              else
              printf("ERROR \n");
            
        }
       else if(choice==2)
        {
                  printf("Album Title=");
                  scanf(" %[^\n]%*c",&albumTitle);
                  printf("%s\n",rut->albumName );
                  rut=removeAlbum(albumTitle,rut);
                      printf("Removed album=");                         
                      
                  
        }
       else if(choice==3){
            showAllAlbums(rut);
        }
       else if(choice==4){
          printf("Album title=");
          scanf(" %[^\n]%*c",&albumTitle);
          if(CONTROL(rut,albumTitle)==1){
            showAlbumDetails(search(rut,albumTitle,albumNTY(rut,albumTitle))->HEADSONG);
          }
          else{
            printf("Not found\n");
          }
        }
        else if(choice==5){
          printf("Album title=");
          scanf(" %[^\n]%*c",&albumTitle);
          printf("Song name=");
          scanf(" %[^\n]%*c",&songName);
          printf("Song length=");
		  scanf("%d",&songLength);
		  if(songLength<=0){
			printf("ERROR\n");
		  }
          else if(CONTROL(rut,albumTitle)==1){
          song *temp = search(rut,albumTitle,albumNTY(rut,albumTitle))->HEADSONG;
          addSong(temp,albumTitle,songName,songLength);
          }
          else{
            printf("ERROR\n");
          }
        }
        else if(choice==6){
          printf("Album title=");
          scanf(" %[^\n]%*c",&albumTitle);
          printf("Song name=");
          scanf(" %[^\n]%*c",&songName);
            if(CONTROL(rut,albumTitle)==1)
                  {
                    song *temp = search(rut,albumTitle,albumNTY(rut,albumTitle))->HEADSONG;
                     search(rut,albumTitle,albumNTY(rut,albumTitle))->HEADSONG =removeSong(albumTitle,songName,temp);
                       printf("Removed album\n");                         
                      
                  }
                  else{
                    printf("Not found \n");
                  }
        }
        else if(choice==7){
          showListofAllSongs(rut);
        }
        else if(choice==8){
          printf("LowerBoundary=");
                  scanf("%d",&LowerBoundary);
                  printf("UpperBoundary=");
                  scanf("%d",&UpperBoundary);
                  showAlbumsInBetweenParticularYears(rut,UpperBoundary,LowerBoundary);
        }
        else if(choice==9){
          printf("LowerBoundary=");
                  scanf("%d",&LowerBoundary);
                  printf("UpperBoundary=");
                  scanf("%d",&UpperBoundary);
                  showSongsWithParticularLength(rut,UpperBoundary,LowerBoundary);
        }
        else{
          break;
        }

        
    }
}
